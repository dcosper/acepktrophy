DELETE FROM `weenie` WHERE `class_Id` = 100000;
DELETE FROM `weenie_properties_int` WHERE `object_Id` = 100000;
DELETE FROM `weenie_properties_bool` WHERE `object_Id` = 100000;
DELETE FROM `weenie_properties_float` WHERE `object_Id` = 100000;
DELETE FROM `weenie_properties_string` WHERE `object_Id` = 100000;
DELETE FROM `weenie_properties_d_i_d` WHERE `object_Id` = 100000;

INSERT INTO `weenie` (`class_Id`, `class_Name`, `type`, `last_Modified`)
VALUES (100000, 'ace100000-proofofconflict', 51, '2020-02-09 13:27:23') /* Stackable */;

INSERT INTO `weenie_properties_int` (`object_Id`, `type`, `value`)
VALUES (100000,   1,     262144) /* ItemType - PromissoryNote */
     , (100000,   5,          1) /* EncumbranceVal */
     , (100000,   8,          0) /* Mass */
     , (100000,   9,          0) /* ValidLocations - None */
     , (100000,  11,        250) /* MaxStackSize */
     , (100000,  12,          1) /* StackSize */
     , (100000,  13,          1) /* StackUnitEncumbrance */
     , (100000,  14,          0) /* StackUnitMass */
     , (100000,  15,        100) /* StackUnitValue */
     , (100000,  16,          1) /* ItemUseable - No */
     , (100000,  19,          0) /* Value */
     , (100000,  53,        101) /* PlacementPosition - Resting */
     , (100000,  93,       1044) /* PhysicsState - Ethereal, IgnoreCollisions, Gravity */;

INSERT INTO `weenie_properties_bool` (`object_Id`, `type`, `value`)
VALUES (100000,  11, True ) /* IgnoreCollisions */
     , (100000,  13, True ) /* Ethereal */
     , (100000,  14, True ) /* GravityStatus */
     , (100000,  19, False ) /* Attackable */
     , (100000,  23, True ) /* DestroyOnSell */
	 , (100000,  69, False ) /* Is Sellable */;

INSERT INTO `weenie_properties_float` (`object_Id`, `type`, `value`)
VALUES (100000,  39, 0.3700000047683716) /* DefaultScale */
     , (100000,  54, 1) /* Use Radius */;

INSERT INTO `weenie_properties_string` (`object_Id`, `type`, `value`)
VALUES (100000,   1, 'Proof of Conflict') /* Name */
     , (100000,  15, 'A notarized document proving your involvement in recent conflict.') /* ShortDesc */
     , (100000,  16, 'A brief, signed document, detailing your victory in conflict, these are considered quite valuable, and if you are able to collect enough of them, you could redeem them for items that can assist you in battle, seek Kiala Rett in Arwic.') /* LongDesc */;

INSERT INTO `weenie_properties_d_i_d` (`object_Id`, `type`, `value`)
VALUES (100000,   1,   33554659) /* Setup */
     , (100000,   3,  536870932) /* SoundTable */
     , (100000,   8,  100692973) /* Icon */
     , (100000,  22,  872415275) /* PhysicsEffectTable */;

/* Lifestoned Changelog:
{
  "LastModified": "2020-02-09T05:24:30.8794302-08:00",
  "ModifiedBy": "Aftertaste",
  "Changelog": [],
  "UserChangeSummary": "test",
  "IsDone": false
}
*/
