DELETE FROM `weenie` WHERE `class_Id` = 100001;
DELETE FROM `weenie_properties_int` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_bool` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_float` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_bool` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_string` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_d_i_d` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_attribute` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_attribute_2nd` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_body_part` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_emote` WHERE `object_Id` = 100001;
DELETE FROM `weenie_properties_create_list` WHERE `object_Id` = 100001;

/* This is not done */

/* Multiple imports of this file will clutter up your emote tables. */

INSERT INTO `weenie` (`class_Id`, `class_Name`, `type`, `last_Modified`)
VALUES (100001, 'proofofconflictvendor', 12, '2005-02-09 10:00:00') /* Vendor */;

INSERT INTO `weenie_properties_int` (`object_Id`, `type`, `value`)
VALUES (100001,   1,         16) /* ItemType - Creature */
     , (100001,   2,         31) /* CreatureType - Human */
     , (100001,   6,         -1) /* ItemsCapacity */
     , (100001,   7,         -1) /* ContainersCapacity */
     , (100001,   8,        120) /* Mass */
     , (100001,  16,         32) /* ItemUseable - Remote */
     , (100001,  25,          275) /* Level */
     , (100001,  27,          0) /* ArmorType - None */
     , (100001,  74,     1208248231) /* ? */
     , (100001,  75,          0) /* MerchandiseMinValue */
     , (100001,  76,      25000) /* MerchandiseMaxValue */
     , (100001,  93,    2098200) /* PhysicsState - ReportCollisions, IgnoreCollisions, Gravity, ReportCollisionsAsEnvironment */
     , (100001, 126,        125) /* VendorHappyMean */
     , (100001, 127,        125) /* VendorHappyVariance */
     , (100001, 133,          4) /* ShowableOnRadar - ShowAlways */
     , (100001, 134,         16) /* PlayerKillerStatus - RubberGlue */
     , (100001, 146,         37) /* XpOverride */;

INSERT INTO `weenie_properties_bool` (`object_Id`, `type`, `value`)
VALUES (100001,   1, True ) /* Stuck */
     , (100001,  12, True ) /* ReportCollisions */
     , (100001,  13, False) /* Ethereal */
     , (100001,  19, False) /* Attackable */
     , (100001,  39, True ) /* DealMagicalItems */
     , (100001,  41, True ) /* ReportCollisionsAsEnvironment */;

INSERT INTO `weenie_properties_float` (`object_Id`, `type`, `value`)
VALUES (100001,   1,       5) /* HeartbeatInterval */
     , (100001,   2,       0) /* HeartbeatTimestamp */
     , (100001,   3,    0.16) /* HealthRate */
     , (100001,   4,       5) /* StaminaRate */
     , (100001,   5,       1) /* ManaRate */
     , (100001,  11,     300) /* ResetInterval */
     , (100001,  13,     0.9) /* ArmorModVsSlash */
     , (100001,  14,       1) /* ArmorModVsPierce */
     , (100001,  15,     1.1) /* ArmorModVsBludgeon */
     , (100001,  16,     0.4) /* ArmorModVsCold */
     , (100001,  17,     0.4) /* ArmorModVsFire */
     , (100001,  18,       1) /* ArmorModVsAcid */
     , (100001,  19,     0.6) /* ArmorModVsElectric */
     , (100001,  37,     0.9) /* BuyPrice */
     , (100001,  38,    0.01) /* SellPrice */
     , (100001,  54,       3) /* UseRadius */
     , (100001,  64,       1) /* ResistSlash */
     , (100001,  65,       1) /* ResistPierce */
     , (100001,  66,       1) /* ResistBludgeon */
     , (100001,  67,       1) /* ResistFire */
     , (100001,  68,       1) /* ResistCold */
     , (100001,  69,       1) /* ResistAcid */
     , (100001,  70,       1) /* ResistElectric */
     , (100001,  71,       1) /* ResistHealthBoost */
     , (100001,  72,       1) /* ResistStaminaDrain */
     , (100001,  73,       1) /* ResistStaminaBoost */
     , (100001,  74,       1) /* ResistManaDrain */
     , (100001,  75,       1) /* ResistManaBoost */
     , (100001, 104,      10) /* ObviousRadarRange */
     , (100001, 125,       1) /* ResistHealthDrain */;

INSERT INTO `weenie_properties_string` (`object_Id`, `type`, `value`)
VALUES (100001,   1, 'Kiala Rett') /* Name */
     , (100001,   3, 'Female') /* Sex */
     , (100001,   4, 'Sho') /* HeritageGroup */
     , (100001,  24, 'Arwic') /* TownName */;

INSERT INTO `weenie_properties_d_i_d` (`object_Id`, `type`, `value`)
VALUES (100001,   1,   33554510) /* Setup */
     , (100001,   2,  150994945) /* MotionTable */
     , (100001,   3,  536870914) /* SoundTable */
     , (100001,   4,  805306368) /* CombatTable */
     , (100001,   8,  100667446) /* Icon */
	 , (100001,  57,  100000) /* Alt Currency */;

INSERT INTO `weenie_properties_attribute` (`object_Id`, `type`, `init_Level`, `level_From_C_P`, `c_P_Spent`)
VALUES (100001,   1,  60, 0, 0) /* Strength */
     , (100001,   2,  60, 0, 0) /* Endurance */
     , (100001,   3,  50, 0, 0) /* Quickness */
     , (100001,   4,  45, 0, 0) /* Coordination */
     , (100001,   5,  25, 0, 0) /* Focus */
     , (100001,   6,  25, 0, 0) /* Self */;

INSERT INTO `weenie_properties_attribute_2nd` (`object_Id`, `type`, `init_Level`, `level_From_C_P`, `c_P_Spent`, `current_Level`)
VALUES (100001,   1,    15, 0, 0, 45) /* MaxHealth */
     , (100001,   3,    15, 0, 0, 75) /* MaxStamina */
     , (100001,   5,    10, 0, 0, 35) /* MaxMana */;

INSERT INTO `weenie_properties_body_part` (`object_Id`, `key`, `d_Type`, `d_Val`, `d_Var`, `base_Armor`, `armor_Vs_Slash`, `armor_Vs_Pierce`, `armor_Vs_Bludgeon`, `armor_Vs_Cold`, `armor_Vs_Fire`, `armor_Vs_Acid`, `armor_Vs_Electric`, `armor_Vs_Nether`, `b_h`, `h_l_f`, `m_l_f`, `l_l_f`, `h_r_f`, `m_r_f`, `l_r_f`, `h_l_b`, `m_l_b`, `l_l_b`, `h_r_b`, `m_r_b`, `l_r_b`)
VALUES (100001,  0,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 1, 0.33,    0,    0, 0.33,    0,    0, 0.33,    0,    0, 0.33,    0,    0) /* Head */
     , (100001,  1,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 2, 0.44, 0.17,    0, 0.44, 0.17,    0, 0.44, 0.17,    0, 0.44, 0.17,    0) /* Chest */
     , (100001,  2,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 3,    0, 0.17,    0,    0, 0.17,    0,    0, 0.17,    0,    0, 0.17,    0) /* Abdomen */
     , (100001,  3,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 1, 0.23, 0.03,    0, 0.23, 0.03,    0, 0.23, 0.03,    0, 0.23, 0.03,    0) /* UpperArm */
     , (100001,  4,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 2,    0,  0.3,    0,    0,  0.3,    0,    0,  0.3,    0,    0,  0.3,    0) /* LowerArm */
     , (100001,  5,  4,  2, 0.75,    0,    0,    0,    0,    0,    0,    0,    0,    0, 2,    0,  0.2,    0,    0,  0.2,    0,    0,  0.2,    0,    0,  0.2,    0) /* Hand */
     , (100001,  6,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 3,    0, 0.13, 0.18,    0, 0.13, 0.18,    0, 0.13, 0.18,    0, 0.13, 0.18) /* UpperLeg */
     , (100001,  7,  4,  0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 3,    0,    0,  0.6,    0,    0,  0.6,    0,    0,  0.6,    0,    0,  0.6) /* LowerLeg */
     , (100001,  8,  4,  2, 0.75,    0,    0,    0,    0,    0,    0,    0,    0,    0, 3,    0,    0, 0.22,    0,    0, 0.22,    0,    0, 0.22,    0,    0, 0.22) /* Foot */;

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.4, NULL, NULL, NULL, NULL, 1 /* Open */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,  10 /* Tell */, 0, 1, NULL, 'Welcome!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.8, NULL, NULL, NULL, NULL, 1 /* Open */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,  10 /* Tell */, 0, 1, NULL, 'Greetings! Watch your back!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.8, NULL, NULL, NULL, NULL, 2 /* Close */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,  10 /* Tell */, 0, 1, NULL, 'Enjoy!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.8, NULL, NULL, NULL, NULL, 3 /* Sell */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,  10 /* Tell */, 0, 1, NULL, 'I am quite pleased, thank you.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.8, NULL, NULL, NULL, NULL, 4 /* Buy */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,  10 /* Tell */, 0, 1, NULL, 'Ah, good choice.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,  0.125, NULL, NULL, NULL, NULL, 5 /* Heartbeat */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,   5 /* Motion */, 0, 1, 318767239 /* Wave */, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,   0.25, NULL, NULL, NULL, NULL, 5 /* Heartbeat */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,   5 /* Motion */, 0, 1, 318767229 /* BowDeep */, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,  0.375, NULL, NULL, NULL, NULL, 5 /* Heartbeat */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,   5 /* Motion */, 0, 1, 318767238 /* Shrug */, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_emote` (`object_Id`, `category`, `probability`, `weenie_Class_Id`, `style`, `substyle`, `quest`, `vendor_Type`, `min_Health`, `max_Health`)
VALUES (100001,  2 /* Vendor */,    0.5, NULL, NULL, NULL, NULL, 5 /* Heartbeat */, NULL, NULL);

SET @parent_id = LAST_INSERT_ID();

INSERT INTO `weenie_properties_emote_action` (`emote_Id`, `order`, `type`, `delay`, `extent`, `motion`, `message`, `test_String`, `min`, `max`, `min_64`, `max_64`, `min_Dbl`, `max_Dbl`, `stat`, `display`, `amount`, `amount_64`, `hero_X_P_64`, `percent`, `spell_Id`, `wealth_Rating`, `treasure_Class`, `treasure_Type`, `p_Script`, `sound`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`, `obj_Cell_Id`, `origin_X`, `origin_Y`, `origin_Z`, `angles_W`, `angles_X`, `angles_Y`, `angles_Z`)
VALUES (@parent_id,  0,   5 /* Motion */, 0, 1, 318767235 /* Nod */, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `weenie_properties_create_list` (`object_Id`, `destination_Type`, `weenie_Class_Id`, `stack_Size`, `palette`, `shade`, `try_To_Bond`)
VALUES (100001, 2,   130,  0, 18, 1, False) /* Create Shirt (130) for Wield */
     , (100001, 2,   127,  0, 16, 1, False) /* Create Pants (127) for Wield */
     , (100001, 2,   30523,  0, 16, 1, False) /* Create Leikotha bracers (30523) for Wield */
	 , (100001, 2,   30520,  0, 16, 1, False) /* Create Leikotha breastplate (30520) for Wield */
	 , (100001, 2,   30525,  0, 16, 1, False) /* Create Leikotha gauntlets (30525) for Wield */
	 , (100001, 2,   30518,  0, 16, 1, False) /* Create Leikotha girth (30518) for Wield */
	 , (100001, 2,   30513,  0, 16, 1, False) /* Create Leikotha greaves (30513) for Wield */
	 , (100001, 2,   30528,  0, 16, 1, False) /* Create Leikotha helm (30528) for Wield */
	 , (100001, 2,   30521,  0, 16, 1, False) /* Create Leikotha pauldrons (30521) for Wield */
	 , (100001, 2,   30516,  0, 16, 1, False) /* Create Leikotha tassets (30516) for Wield */
	 , (100001, 2,   25812,  0, 18, 1, False) /* Create Sollerets Grace (25812) for Wield */
	 /*  Begin shop Inventory*/
	 /* Armour*/
     , (100001, 4,  30510, -1, 0, 0, False) /* Create Adepts Fervor (30510) for Shop */
	 , (100001, 4,  30523, -1, 0, 0, False) /* Create Leikotha's Bracers (30523) for Shop */
	 , (100001, 4,  30520, -1, 0, 0, False) /* Create Leikotha's Breastplate (30520) for Shop */
	 , (100001, 4,  30532, -1, 0, 0, False) /* Create Dusk Coat (30532) for Shop */
	 , (100001, 4,  30530, -1, 0, 0, False) /* Create Dusk Leggings (30530) for Shop */
	 , (100001, 4,  30367, -1, 0, 0, False) /* Create Footmans Boots (30367) for Shop */
	 , (100001, 4,  30534, -1, 0, 0, False) /* Create Guantlets of the Crimson Star (30534) for Shop */
	 , (100001, 4,  30525, -1, 0, 0, False) /* Create Leikotha's Guantlets (30525) for Shop */
	 , (100001, 4,  30524, -1, 0, 0, False) /* Create Geldites Bracers (30524) for Shop */
	 , (100001, 4,  30519, -1, 0, 0, False) /* Create Geldites Breastplate (30519) for Shop */
	 , (100001, 4,  30526, -1, 0, 0, False) /* Create Geldites Gauntlets (30526) for Shop */
	 , (100001, 4,  30517, -1, 0, 0, False) /* Create Geldites Girth (30517) for Shop */
	 , (100001, 4,  30514, -1, 0, 0, False) /* Create Geldites Greaves (30514) for Shop */
	 , (100001, 4,  30511, -1, 0, 0, False) /* Create Geldites Mitre (30511) for Shop */
	 , (100001, 4,  30522, -1, 0, 0, False) /* Create Geldites Pauldrons (30522) for Shop */
	 , (100001, 4,  30515, -1, 0, 0, False) /* Create Geldites Tassets (30515) for Shop */
	 , (100001, 4,  30518, -1, 0, 0, False) /* Create Leikotha's Girth (30518) for Shop */
	 , (100001, 4,  30513, -1, 0, 0, False) /* Create Leikotha's Greaves (30513) for Shop */
	 , (100001, 4,  30528, -1, 0, 0, False) /* Create Leikotha's Helm (30528) for Shop */
	 , (100001, 4,  30512, -1, 0, 0, False) /* Create Chevaird's Helm (30512) for Shop */
	 , (100001, 4,  30533, -1, 0, 0, False) /* Create Patriarch's Coat (30533) for Shop */
	 , (100001, 4,  30532, -1, 0, 0, False) /* Create Patriarch's Tights (30532) for Shop */
	 , (100001, 4,  30521, -1, 0, 0, False) /* Create Leikotha's pauldrons (30521) for Shop */
	 , (100001, 4,  30529, -1, 0, 0, False) /* Create Geldites Boots (30529) for Shop */
	 , (100001, 4,  30368, -1, 0, 0, False) /* Create Steel Wall Boots (30368) for Shop */
	 , (100001, 4,  30516, -1, 0, 0, False) /* Create Leikotha's Tassets (30516) for Shop */
	 , (100001, 4,  30369, -1, 0, 0, False) /* Create Tracker Boots (30369) for Shop */
	 , (100001, 4,  30527, -1, 0, 0, False) /* Create Valkeer's Helm (30527) for Shop */
	 , (100001, 4,  30371, -1, 0, 0, False) /* Create Dread Marauder Shield (30371) for Shop */
	 , (100001, 4,  30373, -1, 0, 0, False) /* Create Mirrored Justice (30373) for Shop */
	 , (100001, 4,  30372, -1, 0, 0, False) /* Create Shield of Engorgement (30372) for Shop */
	 , (100001, 4,  30370, -1, 0, 0, False) /* Create Twin Ward (30370) for Shop */
	 /* End Armour*/
	 
	 /* Spell Gems */
	 , (100001, 4,  30127, -1, 0, 0, False) /* Create Pictograph of Coordination (30127) for Shop */
	 /* End Spell Gems */
	 
	 /* Kits */  
	 , (100001, 4,  30250, -1, 0, 0, False) /* Medicated Health Kit (30250) for Shop */
	 , (100001, 4,  30252, -1, 0, 0, False) /* Medicated Stamina Kit (30252) for Shop */
	 , (100001, 4,  30251, -1, 0, 0, False) /* Medicated Mana Kit (30251) for Shop */
	 /* End Kits */
	 
	 /* Salvage */
	 , (100001, 4,  30102, -1, 0, 0, False) /* Create Foolproof Red Garnet (30102) for Shop */
	 /* End Salvage */
	 
	 /* Magic */
	 , (100001, 4,  30374, -1, 0, 0, False) /* Create Muramm Wand(30374) for Shop */
	 /* End Magic */
	 
	 /*  Jewelry */
     , (100001, 4,  30366, -1, 0, 0, False) /* Create Aristocrats Braclet (30366) for Shop */
	 , (100001, 4,  30354, -1, 0, 0, False) /* Create Elemental Harmony (30354) for Shop */
	 , (100001, 4,  30352, -1, 0, 0, False) /* Create Bracelet Binding(30352) for Shop */
	 , (100001, 4,  30365, -1, 0, 0, False) /* Create Winter's Heart (30365) for Shop */;
