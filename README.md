# ACEPKTrophy

Adds a basic PK trophy system to ACE Asherons Call Emulator


Grab the updated server code https://github.com/ACEmulator/ACE

This is fairly simple code changes

ACE.Server.WorldObjects\Player_Commerce.cs

Here, we factor in SellPrice, which is a float value on your vendor

```
        public void FinalizeBuyTransaction(Vendor vendor, List<WorldObject> uqlist, List<WorldObject> genlist, uint goldcost, uint altcost)
        {
            // vendor accepted the transaction

// Add this code right after this function starts

            if(vendor.AlternateCurrency.HasValue)
            {
                /*
                 * Factor in sellPrice rate, this way you can use regular items, for the most part
                 * This will scale down prices of items, for instance if a item has a value of
                 * 50,000 pyreals, and you scaled the sellPrice of your vendor to 0.01
                 * the item will then cost 500 of the alternate currency, instead of asking 50,000
                 * like before, which forced you to create your own items.
                 *
                 * If you want to bypass this check for any other alternate curency vendors,
                 * meaning, you have created your own items, with the value you want assigned,
                 * then set your sellRate to 1 on the vendor.
                 */

                altcost = (uint)(altcost * vendor.SellPrice ?? 1);

                if (altcost == 0) altcost = 1; // always cost at least 1 alternate currency
            }
```

// End of code to add make sure to add the code above the line listed below to factor correctly

            var valid = ValidateBuyTransaction(vendor, goldcost, altcost);


ACE.Server.WorldObjects\Player_Death.cs

Here we add the rules for the pk drop

find dropItems.AddRange(destroyedItems);
in the public List<WorldObject> CalculateDeathItems(Corpse corpse) function

And paste this right above it

```
			/*
             * To sum this function up, this will grant a proof of conflict drop to the corpse of the player that died
             * this is meant to then be looted by the person who killed them, or if the player can get back to thier body first,
             * then they can loot it as well (built in game mechanic), we then have a vendor that sells items for these trophy's,
             *
             * To be awarded a trophy, you need to meet the following criteria,
             * You must be the same level or higher.
             * You must not be in the same monarchy
             *
             * If you are a lower level you get 2 drops.
             *
             * This really needs expanded to be viable, it does work the way it is, but, it depends on how easy
             * you want players to have access to the items you put on your vendor
             *
             * For instance, if a level 50 player kills a level 100, then they deserve some phat lewt.
             */

            if (IsPKDeath(corpse.KillerId) && corpse.KillerId != Guid.Full) // if we died a pk death and we did not kill ourselves, because that's a sin
            {
                var topDamager = DamageHistory.GetTopDamager(false);

                var player = topDamager.TryGetAttacker() as Player;

                int killerLevel = player.Level ?? 0;

                bool sameMonarch = player?.MonarchId != 0 && corpse?.MonarchId != 0 && player?.MonarchId != corpse?.MonarchId;

                // If the killer level is less or equal to us and not in same monarchy, give pk trophy
                if (killerLevel <= Level && !sameMonarch)
                {
                    /*
                     * Ideally, to stop players from farming, and actually make them fight to gain these drops
                     * you would need additional information, in this case, who killed me in my last pk death,
                     * so if player X killed me last, dont award.
                     *
                     * In addition, we do not award if they are in the same monarchy, but that does not stop them
                     * from breaking, farming, then rejoining.
                     */
                    var pkLoot = WorldObjectFactory.CreateNewWorldObject(100000);

                    if (pkLoot != null)
                    {
                        // We could add a random thing here, like 1-9 , getting 9 trophy's would be rare ?
                        if(killerLevel < Level)
                            pkLoot.SetStackSize(2);

                        if (!corpse.TryAddToInventory(pkLoot))
                        {
                            log.Warn($"Player_Death: couldn't add item to {Name}'s corpse: {pkLoot.Name}");
                        }
                        else
                        {
                            Session.Network.EnqueueSend(new GameMessageSystemChat("Your conflict has been recorded.", ChatMessageType.Broadcast));
                            player.Session.Network.EnqueueSend(new GameMessageSystemChat("Your conflict has been recorded.", ChatMessageType.Broadcast));
                        }
                    }
                }
            }
```


In ace_world database

Import 100000 Proof of Conflict.sql

Import Kiala Rett.sql

Log into game with your admin, go to arwic (because in the description of 100000 Proof Of Conflict.sql it says go see her in arwic)
Stand somewhere in arwic, type @create 100001 and the system will work.